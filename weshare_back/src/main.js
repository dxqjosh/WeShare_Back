import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
// 导入全局样式
import '@/assets/css/global.css'
// 导入axios
import axios from 'axios'

Vue.config.productionTip = false

// 设置默认的URL
axios.default.baseURL = 'http://120.26.179.27:8890'
// axios配置请求拦截器
// axios.interceptors.request.use(config => {
// config.headers.Authorization = window.sessionStorage.getItem('token')
// return config
// })
// 将axios封装为vue的属性
Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
