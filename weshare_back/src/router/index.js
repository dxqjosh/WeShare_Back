import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('@/views/Home.vue'),
    // 二级路由（子路由）的设置
    children: [
      // 请求home时，默认直接重定向到welcome二级路由
      {
        path: '/home',
        redirect: '/welcome'
      },
      {
        path: '/welcome',
        component: () => import('@/views/Welcome.vue')
      },
      {
        path: '/users',
        component: () => import('@/components/Users')
      },
      {
        path: '/managers',
        component: () => import('@/components/Users/managers.vue')
      },
      {
        path: '/resources',
        component: () => import('@/components/Resources')
      },
      {
        path: '/check',
        component: () => import('@/components/Resources/check.vue')
      },
      {
        path: '/askCheck',
        component: () => import('@/components/Resources/askCheck.vue')
      },
      {
        path: '/askList',
        component: () => import('@/components/Resources/askList.vue')
      },
      {
        path: '/report',
        component: () => import('@/components/Report')
      },
      {
        path: '/reportComment',
        component: () => import('@/components/Report/comReport.vue')
      },
      {
        path: '/notice',
        component: () => import('@/components/Notice')
      },
      {
        path: '/editReport',
        component: () => import('@/components/Notice/EditNotice.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// 添加路由守卫  beforeEach导航守卫
// to  表示将要访问你的路径 form 从哪来  next 是下一个要进行的操作
router.beforeEach((to, from, next) => {
  // 对用户访问的路由进行拦截， 如果访问的是登录页 那么这是就直接放行
  if (to.path === '/login') {
    return next() // 放行
  }
  // 如果访问的不是登录页 则我们需要来检测用户是否登陆 如果登陆 则直接放行  否则 就跳转到登录页
  const token = window.sessionStorage.getItem('token')
  if (!token) {
    // 没有token
    return next('/login')
  }
  next()
})

export default router
